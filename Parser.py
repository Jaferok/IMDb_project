import gzip
import sqlite3
import os
from urllib.request import urlopen
from contextlib import contextmanager
from random import choice
from datetime import date


BASE_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "Parser.py")[:-9]
ENTRY_QUESTION_FILMS = ("film", "Film", "F", "f", "1")
ENTRY_QUESTION_ACTORS = ("actor", "Actor", "a", "A", "2")
ENTRY_QUESTION_TOP = ("Top", "t", "T", "top", "3")
ENTRY_QUESTION_DOWNLOAD_DB = ("download db", "download", "5")
ENTRY_QUESTION_CREATE_SQLITE = ("sqlite", "make db", "6")
ENTRY_QUESTION_RANDOM = ("random", "4", "r")
URL_FOR_FILM = "https://www.imdb.com/title/"
URL_FOR_ACTOR = "https://www.imdb.com/name/"

DB_URLS = [
    "https://datasets.imdbws.com/name.basics.tsv.gz",
    "https://datasets.imdbws.com/title.akas.tsv.gz",
    "https://datasets.imdbws.com/title.basics.tsv.gz",
    "https://datasets.imdbws.com/title.crew.tsv.gz",
    "https://datasets.imdbws.com/title.episode.tsv.gz",
    "https://datasets.imdbws.com/title.principals.tsv.gz",
    "https://datasets.imdbws.com/title.ratings.tsv.gz"
]


def first_start():

    print("Hi, let's check some important files to continue working.")

    try:

        open(BASE_PATH + "main.db")

    except IOError as e:

        download_question = input("Database doesn't exist yet. I think it's time to download files and create database(that will last about 30 mins). Yes or Not ?")

        if download_question.lower().startswith("y"):

            db = DB_creator()
            load_db()
            db.make_main_db()

        else:

            print("Sorry, but we can't work without this database =(")
            quit()

    else:

            print("It's ok db here.")


def entry_question():

    entry_question = input("What do you want to find ?" + "\n" +
                           "1. Film by title name. " + "\n" +
                           "2. Actor by actor name. " + "\n" +
                           "3. Get top films. " + "\n" +
                           "4. Get random films. " + "\n" +
                           "5. Delete list with past films. " + "\n" +
                           ">>>>>")

    DB = DB_creator()
    Finder = DB_connector(DB)
    Sql_req = SQL_requests(DB)

    if entry_question in ENTRY_QUESTION_FILMS:

        input_film = input("Enter film >>> ")
        input_film = input_film.capitalize()

        original = input("It's original title ?(Yes or Not)>>> ")

        original = original.lower().startswith("y")

        time = input("Enter film years from to, like this 1990, 2000 (if you remember it's for sure, you can right only one year)>>> ")

        start_year_input, *end_year_input = time.split(",")
        end_year_input = end_year_input[0] if end_year_input else start_year_input


        Finder.get_film_id(input_film, original, time, start_year_input, end_year_input)


    elif entry_question in ENTRY_QUESTION_ACTORS:

        input_actor = input("Enter First Name and Last Name of actor(Like Fred Astaire)>>> ")
        input_actor = input_actor.title()

        see_all_films_question = input("Do you want to see all titles with this actor or just known titles(Yes or Not)?>>> ")
        see_all_films_question = see_all_films_question.lower().startswith("y")

        Finder.get_name_const_and_info(input_actor, see_all_films_question)


    elif entry_question in ENTRY_QUESTION_TOP:

        input_top = input("Enter number of films, what's the top(movie,tvSeries,short, etc.), and genre(Like this: 10 movie comedy)")
        input_top = input_top.split(' ')
        input_top[1] = input_top[1].capitalize()

        number_of_films = int(input_top[0])
        title_type_of_top = input_top[1]
        genre = input_top[2]

        Finder.get_top_films(number_of_films, genre, title_type_of_top)

    elif entry_question in ENTRY_QUESTION_RANDOM:

        input_random = input("Enter number of films, genre, title type(movie,tvSeries,short, etc.), how much known title must be(It's scaling from number of votes, you can type number)")
        input_random_splitted = input_random.split(" ")

        created_at = str(date.today())
        number_of_random_films = int(input_random_splitted[0])
        genre_of_random_films = input_random_splitted[1]
        title_type_of_random_films = input_random_splitted[2]
        fame_of_random_films = input_random_splitted[3]

        try:
            past_req = Sql_req.check_past_request(genre_of_random_films, title_type_of_random_films)
            Sql_req.insert_past_request(input_random, created_at)
            search_id = Sql_req.DB.cursor.lastrowid

        except IndexError:

            Sql_req.insert_past_request(input_random, created_at)
            search_id = Sql_req.DB.cursor.lastrowid

        if past_req:

            list_of_repeat_films_id = Sql_req.show_repeat_titles(title_type_of_random_films, genre_of_random_films)
            len_list_of_repeat_films = len(list_of_repeat_films_id)
            len_random_films = Finder.len_random_films(genre_of_random_films, fame_of_random_films, title_type_of_random_films)

            if len_list_of_repeat_films >= len_random_films:

                print("This is all movies with such a request. ")
                quit()


            repeat_question = input("You already have seen " + str(len_list_of_repeat_films) + " from possible " + str(len_random_films) +
                                    ". Do you want to see exactly past films or the remaining films ?(p or r)")

            if repeat_question.lower().startswith("p"):

                list_of_repeat_films_id = Sql_req.show_repeat_titles(title_type_of_random_films, genre_of_random_films)
                Sql_req.insert_past_request(input_random, created_at)


                Finder.repeat_films(list_of_repeat_films_id)

            else:

                Finder.get_remaining_films(list_of_repeat_films_id, title_type_of_random_films, genre_of_random_films, fame_of_random_films, search_id, number_of_random_films)


        else:

            Finder.get_random_films(number_of_random_films, genre_of_random_films, fame_of_random_films, title_type_of_random_films, search_id)

    elif entry_question == "5":

        Sql_req.delete_past_films_and_requests()



def load_db():																			# Load all DB needed in programm

    for row in DB_URLS:
        file_name = row[28:]
        file_name_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), file_name)


        try:
            f = open(file_name_path)
        except IOError:
            print('Dowloading DB ' + file_name)
            db = urlopen(row).read()
            with open(file_name_path, "wb") as f:
                f.write(db)
        else:
            print("It's OK, you already have this DB." + file_name)
    print("All DB's was downloaded succesfully.")


@contextmanager
def create_table(filename):

    opened_file = None
    try:
        with gzip.open(os.path.join(BASE_PATH, filename), "rt", encoding="utf-8") as f:
            opened_file = f.readlines()
    except FileNotFoundError:
        print("You didn't download gz archive.")
        return

    yield opened_file

    os.remove(BASE_PATH + filename)


class Clarifying_question:

    def continue_question(self):

        continue_question = input("If you want more, just press enter, else type q.>>> \n")

        return continue_question


    def num_of_films_for_print(self, len_all_films_with_actor):

        try:
            num_of_films_for_print = int(input("This actor has {} titles \nHow much films do you want to see at one output ?\n".format(len_all_films_with_actor)))
        except ValueError:
            num_of_films_for_print = 5

        return num_of_films_for_print


    def only_movies_question(self):

        only_movies_question = input("Do you want to see all list or you can write concrete type ?(Type a - all or movie)>>> ")

        return only_movies_question


class DB_creator:

    DB_NAME = "main.db"

    def __init__(self):

        self.conn = sqlite3.connect(os.path.join(BASE_PATH, self.DB_NAME))
        self.cursor = self.conn.cursor()


    def make_name_basics_table(self):

        with create_table("name.basics.tsv.gz") as opened_file:


            self.cursor.execute("""CREATE TABLE name_basics (
                                                nconst text,
                                                primaryName text,
                                                birthYear integer,
                                                deathYear integer,
                                                primaryProfession text,
                                                knownForTitles text
                                                )""")

            for row in opened_file:
                splitted = row.split('\t')
                nconst = splitted[0]
                primaryName = splitted[1]

                try:
                    birthYear = int(splitted[2])
                except ValueError:
                    birthYear = 0

                try:
                    deathYear = int(splitted[3])
                except ValueError:
                    deathYear = 0

                primaryProfession = splitted[4]
                knownForTitles = splitted[5]

                self.cursor.execute("INSERT INTO name_basics VALUES (:nconst, :primaryName, :birthYear, :deathYear, :primaryProfession, :knownForTitles)",
                                    {"nconst": nconst,
                                     "primaryName": primaryName,
                                     "birthYear": birthYear,
                                     "deathYear": deathYear,
                                     "primaryProfession": primaryProfession,
                                     "knownForTitles": knownForTitles})

            self.conn.commit()



            print("Part 1 of 7 is succesfully completed")


    def make_title_akas_table(self):

        with create_table("title.akas.tsv.gz") as opened_file:

            self.cursor.execute("""CREATE TABLE title_akas(
                                                titleId text,
                                                ordering integer,
                                                title text,
                                                region text,
                                                language text,
                                                types text,
                                                attributes text,
                                                isOriginalTitle integer
                                                )""")

            for row in opened_file:

                splitted = row.split("\t")
                titleId = splitted[0]

                try:
                    ordering = int(splitted[1])
                except ValueError:
                    ordering = 0

                title = splitted[2]
                region = splitted[3]
                language = splitted[4]
                types = splitted[5]
                attributes = splitted[6]

                try:
                    isOriginalTitle = int(splitted[7])
                except ValueError:
                    isOriginalTitle = 0

                self.cursor.execute("INSERT INTO title_akas VALUES (:titleId, :ordering, :title, :region, :language, :types, :attributes, :isOriginalTitle)",
                                    {"titleId": titleId,
                                     "ordering": ordering,
                                     "title": title,
                                     "region": region,
                                     "language": language,
                                     "types": types,
                                     "attributes": attributes,
                                     "isOriginalTitle": isOriginalTitle})

            self.conn.commit()




            print("Part 2 of 7 is succesfully completed")


    def make_title_basics_table(self):

        with create_table("title.basics.tsv.gz") as opened_file:

            self.cursor.execute("""CREATE TABLE title_basics (
                                                tconst text,
                                                titleType text,
                                                primaryTitle text,
                                                originalTitle text,
                                                isAdult integer,
                                                startYear integer,
                                                endYear integer,
                                                runtimeMinutes integer,
                                                genres text
                                                )""",)

            for row in opened_file:

                splitted = row.split("\t")
                tconst = splitted[0]
                titleType = splitted[1]
                primaryTitle = splitted[2]
                originalTitle = splitted[3]

                try:
                    isAdult = int(splitted[4])
                except ValueError:
                    isAdult = 2

                try:
                    startYear = int(splitted[5])
                except ValueError:
                    startYear = 0

                try:
                    endYear = int(splitted[6])
                except ValueError:
                    endYear = 0

                try:
                    runtimeMinutes = int(splitted[7])
                except ValueError:
                    runtimeMinutes = 0

                genres = splitted[8]

                self.cursor.execute("INSERT INTO title_basics VALUES (:tconst, :titleType, :primaryTitle, :originalTitle, :isAdult, :startYear, :endYear, :runtimeMinutes, :genres)",
                                    {"tconst": tconst,
                                     "titleType": titleType,
                                     "primaryTitle": primaryTitle,
                                     "originalTitle": originalTitle,
                                     "isAdult": isAdult,
                                     "startYear": startYear,
                                     "endYear": endYear,
                                     "runtimeMinutes": runtimeMinutes,
                                     "genres": genres})

            self.conn.commit()



            print("Part 3 of 7 is succesfully completed")


    def make_crew_table(self):

        with create_table("title.crew.tsv.gz") as opened_file:

            self.cursor.execute("""CREATE TABLE title_crew (
                                                tconst text,
                                                directors text,
                                                writers text
                                                )""")

            for row in opened_file:

                splitted = row.split("\t")
                tconst = splitted[0]
                directors = splitted[1]
                writers = splitted[2]

                self.cursor.execute("INSERT INTO title_crew VALUES (:tconst, :directors, :writers)",
                                    {"tconst": tconst,
                                     "directors": directors,
                                     "writers": writers})

            self.conn.commit()



            print("Part 4 of 7 is succesfully completed")


    def make_title_episode_table(self):

        with create_table("title.episode.tsv.gz") as opened_file:

            self.cursor.execute("""CREATE TABLE title_episode (
                                                tconst text,
                                                parentTconst text,
                                                seasonNumber integer,
                                                episodeNumber integer
                                                )""")

            for row in opened_file:

                splitted = row.split("\t")
                tconst = splitted[0]
                parentTconst = splitted[1]
                try:
                    seasonNumber = splitted[2]
                except ValueError:
                    seasonNumber = 0

                try:
                    episodeNumber = splitted[3]
                except ValueError:
                    episodeNumber = 0

                self.cursor.execute("INSERT INTO title_episode VALUES (:tconst, :parentTconst, :seasonNumber, :episodeNumber)",
                                    {"tconst": tconst,
                                     "parentTconst": parentTconst,
                                     "seasonNumber": seasonNumber,
                                     "episodeNumber": episodeNumber})

            self.conn.commit()


            print("Part 5 of 7 is succesfully completed")


    def make_principals_table(self):

        with create_table("title.principals.tsv.gz") as opened_file:

            self.cursor.execute("""CREATE TABLE title_principals (
                                                tconst text,
                                                ordering integer,
                                                nconst text,
                                                category text,
                                                job text,
                                                characters text
                                                )""")

            for row in opened_file:

                splitted = row.split("\t")
                tconst = splitted[0]

                try:
                    ordering = splitted[1]
                except ValueError:
                    ordering = 0

                nconst = splitted[2]
                category = splitted[3]
                job = splitted[4]
                characters = splitted[5]

                self.cursor.execute("INSERT INTO title_principals VALUES (:tconst, :ordering, :nconst, :category, :job, :characters)",
                                    {"tconst": tconst,
                                     "ordering": ordering,
                                     "nconst": nconst,
                                     "category": category,
                                     "job": job,
                                     "characters": characters})

            self.conn.commit()


            print("Part 6 of 7 is succesfully completed")


    def make_title_rating_table(self):

        with create_table("title.ratings.tsv.gz") as opened_file:

            self.cursor.execute("""CREATE TABLE title_ratings (
                                                tconst text,
                                                averageRating real,
                                                numVotes integer
                                                )""")

            for row in opened_file:

                splitted = row.split("\t")
                tconst = splitted[0]

                try:
                    averageRating = float(splitted[1])
                except ValueError:
                    averageRating = 0

                try:
                    numVotes = int(splitted[2])
                except ValueError:
                    numVotes = 0

                self.cursor.execute("INSERT INTO title_ratings VALUES (:tconst, :averageRating, :numVotes)",
                                    {"tconst": tconst,
                                     "averageRating": averageRating,
                                     "numVotes": numVotes})

                print("Part 7 of 7 is succesfully completed")

            self.conn.commit()

    def make_requests_table(self):

        self.cursor.execute("""CREATE TABLE past_requests (
                                            id integer PRIMARY KEY,
                                            string_request text,
                                            created_at text
                                            )""")


    def make_past_films_table(self):

        self.cursor.execute("""CREATE TABLE past_films (
                                            id integer PRIMARY KEY,
                                            film_id text,
                                            search_id integer
                                            )""")


    def make_main_db(self):

        self.make_name_basics_table()
        self.make_title_akas_table()
        self.make_title_basics_table()
        self.make_crew_table()
        self.make_title_episode_table()
        self.make_principals_table()
        self.make_title_rating_table()
        self.make_requests_table()
        self.make_past_films_table()

        print("Aaaand it's gone!")


class SQL_requests:


    def __init__(self, DB):

        self.DB = DB

    def get_list_of_films_id(self, input_film, original):

        input_film_for_request = "%{0}%".format(input_film)

        unfinished_tt_consts = self.DB.cursor.execute("""SELECT titleId
                                                         FROM title_akas
                                                         WHERE title LIKE :input_film_for_request and
                                                         isOriginalTitle = :original """,
                                                      {"input_film_for_request": input_film_for_request,
                                                       "original": original})

        return unfinished_tt_consts


    def get_rows_with_cinema_info(self, tt_consts, start_year_input, end_year_input):

        rows_with_cinema_info = self.DB.cursor.execute("""SELECT * FROM title_basics
                                                          LEFT JOIN title_ratings
                                                          ON title_basics.tconst = title_ratings.tconst
                                                          WHERE title_basics.tconst IN %(tt_consts)s
                                                          AND startYear BETWEEN %(start_year_input)s
                                                          AND %(end_year_input)s """ %
                                                       {"tt_consts": tt_consts,
                                                        "start_year_input": start_year_input,
                                                        "end_year_input": end_year_input})

        rows_with_cinema_info = list(rows_with_cinema_info)

        return rows_with_cinema_info


    def get_list_of_actors(self, input_actor):

        input_actor_for_request = "%{}%".format(input_actor)

        list_of_actors = self.DB.cursor.execute("""SELECT * FROM name_basics
                                                WHERE primaryName
                                                LIKE :input_actor_for_request """,
                                                {"input_actor_for_request": input_actor_for_request})

        list_of_actors = list(list_of_actors)

        return list_of_actors


    def get_all_films_with_actor(self, id_on_imdb):

        all_films_with_actor = self.DB.cursor.execute("""SELECT * FROM title_principals
                                                         WHERE title_principals.nconst = :id_on_imdb""",
                                                      {"id_on_imdb": id_on_imdb})

        all_films_with_actor = list(all_films_with_actor)

        return all_films_with_actor


    def get_row_with_cinema_info_with_title_type(self, tconst, title_type):

        row_with_cinema_info_all = self.DB.cursor.execute("""SELECT * FROM title_basics
                                                            LEFT JOIN title_ratings
                                                            ON title_basics.tconst = title_ratings.tconst
                                                            WHERE title_basics.tconst = :tconst
                                                            AND title_basics.titleType = :title_type""",
                                                          {"tconst": tconst,
                                                           "title_type": title_type})

        return row_with_cinema_info_all


    def get_row_with_cinema_info_with_all_title_types(self, tconst):

        row_with_cinema_info_all = self.DB.cursor.execute("""SELECT * FROM title_basics
                                                             LEFT JOIN title_ratings
                                                             ON title_basics.tconst = title_ratings.tconst
                                                             WHERE title_basics.tconst = :tconst""",
                                                          {"tconst": tconst})

        return row_with_cinema_info_all


    def get_known_actor_titles(self, tuple_known_for_titles):

        rows_with_cinema_info = self.DB.cursor.execute("""SELECT * FROM title_basics
                                                          LEFT JOIN title_ratings
                                                          ON title_basics.tconst = title_ratings.tconst
                                                          WHERE title_basics.tconst IN %(tuple_known_for_titles)s """ %
                                                       {"tuple_known_for_titles": tuple_known_for_titles})

        return rows_with_cinema_info

    def get_list_of_top_films(self, number_of_films, genre, title_type_of_top):

        genre_for_req = "%{}%".format(genre)
        title_type_of_top_for_req = "%{}%".format(title_type_of_top)

        top_films = self.DB.cursor.execute("""SELECT * FROM title_basics
                                           LEFT JOIN title_ratings
                                           ON title_basics.tconst = title_ratings.tconst
                                           WHERE genres LIKE :genre_for_req AND numVotes > 25000
                                           AND titleType LIKE :title_type_of_top_for_req
                                           ORDER BY averageRating DESC """,
                                           {"genre_for_req": genre_for_req,
                                            "title_type_of_top_for_req": title_type_of_top_for_req})

        top_films = list(top_films.fetchmany(number_of_films))

        return top_films


    def get_list_of_random_films(self, genre_of_random_films, fame_of_random_films, title_type_of_random_films):

        genre_of_random_films_for_req = "%{}%".format(genre_of_random_films)
        title_type_of_random_films_for_req = "%{}%".format(title_type_of_random_films)


        random_films = self.DB.cursor.execute("""SELECT * FROM title_basics
                                              LEFT JOIN title_ratings
                                              ON title_basics.tconst = title_ratings.tconst
                                              WHERE genres LIKE :genre_of_random_films_for_req
                                              AND titleType LIKE :title_type_of_random_films_for_req
                                              AND numVotes >= :fame_of_random_films""",
                                              {"genre_of_random_films_for_req": genre_of_random_films_for_req,
                                               "title_type_of_random_films_for_req": title_type_of_random_films_for_req,
                                               "fame_of_random_films": fame_of_random_films})

        random_films = list(random_films)

        return random_films


    def check_past_films(self, ttconst_for_movies_list):

        past_film = self.DB.cursor.execute("""SELECT * FROM past_films
                                              WHERE film_id LIKE :ttconst_for_movies_list""",
                                           {"ttconst_for_movies_list": ttconst_for_movies_list})

        return past_film


    def check_past_request(self, genre_of_random_films, title_type_of_random_films):

        genre = "%{}%".format(genre_of_random_films)
        title_type = "%{}%".format(title_type_of_random_films)

        past_request = self.DB.cursor.execute("""SELECT * FROM past_requests
                                                 WHERE string_request LIKE :genre AND
                                                 string_request LIKE :title_type""",
                                              {"genre": genre,
                                               "title_type": title_type})

        past_request = past_request.fetchall()

        return past_request

    def insert_past_request(self, input_random, created_at):

        self.DB.cursor.execute("""INSERT INTO past_requests(string_request, created_at)
                                  VALUES (:input_random, :created_at)""",
                               {"input_random": input_random,
                                "created_at": created_at})

        self.DB.conn.commit()


    def insert_past_films(self, tt_const_for_movies_list, search_id):

        self.DB.cursor.execute("""INSERT INTO past_films(film_id, search_id)
                                  VALUES (:ttconst_for_movies_list, :search_id)""",
                               {"ttconst_for_movies_list": tt_const_for_movies_list,
                                "search_id": search_id})

        self.DB.conn.commit()

    def show_repeat_titles(self, title_type_of_random_films, genre_of_random_films):

        genre = "%{}%".format(genre_of_random_films)
        title_type = "%{}%".format(title_type_of_random_films)

        list_of_repeat_films_id = self.DB.cursor.execute("""SELECT film_id FROM past_films
                                                            LEFT JOIN past_requests ON past_films.search_id = past_requests.id
                                                            WHERE past_requests.id = past_films.search_id AND
                                                            string_request LIKE :genre AND
                                                            string_request LIKE :title_type""",
                                                         {"genre": genre,
                                                          "title_type": title_type})

        list_of_repeat_films_id = list_of_repeat_films_id.fetchall()

        return list_of_repeat_films_id


    def delete_past_films_and_requests(self):

        self.DB.cursor.execute("DELETE FROM past_films")
        self.DB.cursor.execute("DELETE FROM past_requests")

        self.DB.conn.commit()
        print("Deleted.")



class Output_for_user:

    def output_for_film(self, film_id, title_type,
                        primary_title, original_title,
                        release_date, length, genres,
                        rate, number_of_votes, url_for_print):

        print("Was found " + primary_title +
              "\nFilm id on IMDb - " + film_id +
              "\noriginal title - " + original_title +
              " and it's " + title_type +
              "\nrelease date - " + release_date +
              "\nlength - " + length + " minutes" +
              "\ngenres: " + genres +
              "\nRating: " + rate +
              "\nURL on IMDb - " + url_for_print +
              "\nNumber of votes: " + number_of_votes + "\n")


    def output_for_actor(self, primary_name, id_on_imdb,
                         birth_year, death_year, primary_professions,
                         url_actor_for_print):

        print("Was found " + primary_name +
              "\nId on IMDb - " + id_on_imdb +
              "\nBirth year - " + birth_year +
              "\nDeath year - " + death_year +
              "\nPrimary Professions :  " + primary_professions +
              "\nURL on IMDb - " + url_actor_for_print +
              "\nKnown for titles: " + "\n")


    def output_for_all_actor_films(self, primary_title, film_id,
                                   original_title, title_type, release_date,
                                   length, genres, category,
                                   job, characters, rate,
                                   number_of_votes, url_for_print):

        print("\nWas found " + primary_title +
              "\nFilm id on IMDb - " + film_id +
              "\noriginal title - " + original_title +
              " and it's " + title_type +
              "\nrelease date - " + release_date +
              "\nlength - " + length + " minutes" +
              "\ngenres: " + genres +
              "\nActor job's category - " + category +
              "\nActor job's - " + job +
              "\nActor's characters - " + characters +
              "\nRating: " + rate +
              "\nNumber of votes: " + number_of_votes +
              "\nURL on IMDb - " + url_for_print +
              "\n**********")


class DB_connector:

    output_for_user = Output_for_user()
    clarifying_question = Clarifying_question()

    def __init__(self, DB):

        self.SQL_req = SQL_requests(DB)

    def get_film_id(self, input_film, original, time, start_year_input, end_year_input):								# Finding tt_const of film in DB

        unfinished_tt_consts = self.SQL_req.get_list_of_films_id(input_film, original)

        unfinished_tt_consts = list(unfinished_tt_consts)

        tt_consts = []

        for const in unfinished_tt_consts:
            const = str(const)
            const = const[2:-3]
            tt_consts.append(const)


        if len(tt_consts) == 0:
            print("Didn't find anything like " + input_film)
            quit()
        elif len(tt_consts) == 1:
            tt_consts.append(tt_consts[0])

        print('Was found ' + str(len(set(tt_consts))) + ' films contains ' + input_film + ' in title.' + '\n')

        tt_consts = tuple(tt_consts)


        self.get_cinema_info(tt_consts, time, input_film, start_year_input, end_year_input)


    def get_cinema_info(self, tt_consts, time, input_film, start_year_input, end_year_input):									   # Get some info about film with tt_const

        rows_with_cinema_info = self.SQL_req.get_rows_with_cinema_info(tt_consts, start_year_input, end_year_input)

        if len(rows_with_cinema_info) == 0:
            print("Didn't find any films with " + input_film + " in title " + "at " + time + " years.")
            quit()
        else:
            for film_row_split in rows_with_cinema_info:

                film_id = film_row_split[0]
                title_type = film_row_split[1]
                primary_title = film_row_split[2]
                original_title = film_row_split[3]
                # is_adult = film_row_split[4]
                release_date = str(film_row_split[5])
                # end_year = str(film_row_split[6])
                length = str(film_row_split[7])
                genres = film_row_split[8]
                rate = str(film_row_split[10])
                number_of_votes = str(film_row_split[11])
                url_for_print = URL_FOR_FILM + film_id + "/"


                self.output_for_user.output_for_film(film_id, title_type, primary_title,
                                                     original_title, release_date, length,
                                                     genres, rate, number_of_votes, url_for_print)


    def get_name_const_and_info(self, input_actor, see_all_films_question):

        list_of_actors = self.SQL_req.get_list_of_actors(input_actor)

        if len(list_of_actors) == 0:
            print("Didn't find any actors with " + input_actor + " in name.")
            quit()
        else:
            print("Was found " + str(len(list_of_actors)) + " names contains " + input_actor + " .\n")

        self.get_actor_films(list_of_actors, see_all_films_question)


    def get_actor_films(self, list_of_actors, see_all_films_question):

        for actor in list_of_actors:

            see_all_films_question_for_func = see_all_films_question

            id_on_imdb = actor[0]
            primary_name = actor[1]
            birth_year = str(actor[2])

            if birth_year == "0":
                birth_year = "Unknown"

            death_year = str(actor[3])

            if death_year == "0" and birth_year == "Unknown":
                death_year = "Unkwown"
            elif death_year == "0":
                death_year = "Is alive and well"

            primary_professions = actor[4]
            known_for_titles = actor[5]
            url_actor_for_print = URL_FOR_ACTOR + id_on_imdb + "/"

            known_for_titles = known_for_titles.split(",")
            known_for_titles[-1] = known_for_titles[-1][:-1]

            if len(known_for_titles) == 1:
                known_for_titles.append(known_for_titles[0])

            tuple_known_for_titles = tuple(known_for_titles)

            self.output_for_user.output_for_actor(primary_name, id_on_imdb,
                                                  birth_year, death_year, primary_professions,
                                                  url_actor_for_print)

            if tuple_known_for_titles[0].startswith("\\"):

                print("Doesn't have any titles.\n")
                continue

            if "actor" not in primary_professions or len(tuple_known_for_titles) < 4:
                see_all_films_question_for_func = 0


            if see_all_films_question_for_func == 1:

                all_films_with_actor = self.SQL_req.get_all_films_with_actor(id_on_imdb)

                len_all_films_with_actor = str(len(all_films_with_actor))

                num_of_films_for_print = self.clarifying_question.num_of_films_for_print(len_all_films_with_actor)
                only_movies_question = self.clarifying_question.only_movies_question()

                num_of_films_in_cycle = 0
                num_of_films_in_cycle_for_sum = num_of_films_for_print
                break_question = 0


                for row_with_one_cinema in all_films_with_actor:

                    if break_question == 1:
                        break

                    tconst = row_with_one_cinema[0]
                    category = row_with_one_cinema[3]
                    job = row_with_one_cinema[4]
                    characters = row_with_one_cinema[5]


                    if only_movies_question == "a":

                        row_with_cinema_info_all = self.SQL_req.get_row_with_cinema_info_with_all_title_types(tconst)

                    else:

                        title_type = only_movies_question

                        row_with_cinema_info_all = self.SQL_req.get_row_with_cinema_info_with_title_type(tconst, title_type)

                    row_with_cinema_info_all = list(row_with_cinema_info_all)

                    for film_row in row_with_cinema_info_all:

                        film_id = film_row[0]
                        title_type = film_row[1]
                        primary_title = film_row[2]
                        original_title = film_row[3]
                        # is_adult = film_row[4]
                        release_date = str(film_row[5])
                        # end_year = str(film_row[6])
                        length = str(film_row[7])
                        genres = film_row[8]
                        rate = str(film_row[10])
                        number_of_votes = str(film_row[11])
                        url_for_print = URL_FOR_FILM + film_id + "/"

                        self.output_for_user.output_for_all_actor_films(primary_title, film_id,
                                                                        original_title, title_type, release_date,
                                                                        length, genres, category,
                                                                        job, characters, rate,
                                                                        number_of_votes, url_for_print)

                        num_of_films_in_cycle += 1

                        if num_of_films_in_cycle == int(len_all_films_with_actor):

                            print("That's all films with this actor\n" + "*********")
                            break_question = 1
                            break

                        if num_of_films_in_cycle == num_of_films_in_cycle_for_sum:

                            continue_question = self.clarifying_question.continue_question()

                            if continue_question == "q":

                                break_question = 1
                                break
                            else:

                                num_of_films_in_cycle_for_sum += num_of_films_for_print
                                continue


            else:
                rows_with_cinema_info = self.SQL_req.get_known_actor_titles(tuple_known_for_titles)

                rows_with_cinema_info = list(rows_with_cinema_info)

                if len(rows_with_cinema_info) == 0:
                    print("Doesn't have any titles.\n")


                for film_row in rows_with_cinema_info:


                    film_id = film_row[0]
                    title_type = film_row[1]
                    primary_title = film_row[2]
                    original_title = film_row[3]
                    # is_adult = film_row[4]
                    release_date = str(film_row[5])
                    # end_year = str(film_row[6])
                    length = str(film_row[7])
                    genres = film_row[8]
                    rate = str(film_row[10])
                    number_of_votes = str(film_row[11])
                    url_for_print = URL_FOR_FILM + film_id + "/"

                    self.output_for_user.output_for_film(film_id, title_type, primary_title,
                                                         original_title, release_date, length,
                                                         genres, rate, number_of_votes, url_for_print)


    def get_top_films(self, genre, number_of_films, title_type_of_top):

        top_films = self.SQL_req.get_list_of_top_films(genre, number_of_films, title_type_of_top)

        for film_row in top_films:

            film_id = film_row[0]
            title_type = film_row[1]
            primary_title = film_row[2]
            original_title = film_row[3]
            # is_adult = film_row[4]
            release_date = str(film_row[5])
            # end_year = str(film_row[6])
            length = str(film_row[7])
            genres = film_row[8]
            rate = str(film_row[10])
            number_of_votes = str(film_row[11])
            url_for_print = URL_FOR_FILM + film_id + "/"

            self.output_for_user.output_for_film(film_id, title_type, primary_title,
                                                 original_title, release_date, length,
                                                 genres, rate, number_of_votes, url_for_print)

    def get_random_films(self, number_of_random_films, genre_of_random_films, fame_of_random_films, title_type_of_random_films, search_id):

        random_films = self.SQL_req.get_list_of_random_films(genre_of_random_films, fame_of_random_films, title_type_of_random_films)

        len_random_films = len(random_films)

        if number_of_random_films > len_random_films:

            print("We have only " + str(len_random_films) + " titles, and the output will be exactly this. ")

            number_of_random_films = len_random_films

        number_of_random_films_for_cycle = 0
        num_for_break_cycle = 0
        sum_number_of_random_films = number_of_random_films


        while number_of_random_films_for_cycle != sum_number_of_random_films:

            random_choice = choice(random_films)
            ttconst_for_movies_list = random_choice[0]


            movies_list = self.SQL_req.check_past_films(ttconst_for_movies_list)

            if num_for_break_cycle == len_random_films:

                print("That's all.")
                break

            if movies_list.fetchall():

                num_for_break_cycle += 1
                continue

            else:


                    film_id = random_choice[0]
                    title_type = random_choice[1]
                    primary_title = random_choice[2]
                    original_title = random_choice[3]
                    release_date = str(random_choice[5])
                    length = str(random_choice[7])
                    genres = random_choice[8]
                    rate = str(random_choice[10])
                    number_of_votes = str(random_choice[11])
                    url_for_print = URL_FOR_FILM + film_id + "/"

                    self.output_for_user.output_for_film(film_id, title_type, primary_title,
                                                         original_title, release_date, length,
                                                         genres, rate, number_of_votes, url_for_print)

                    number_of_random_films_for_cycle += 1

                    self.SQL_req.insert_past_films(ttconst_for_movies_list, search_id)

                    if number_of_random_films_for_cycle >= sum_number_of_random_films:

                        continue_question = self.clarifying_question.continue_question()

                        if continue_question == "q":
                            print("I'll be back")
                            break
                        else:
                            sum_number_of_random_films += number_of_random_films
                            continue


    def len_random_films(self, genre_of_random_films, fame_of_random_films, title_type_of_random_films):

        random_films = self.SQL_req.get_list_of_random_films(genre_of_random_films, fame_of_random_films, title_type_of_random_films)

        len_random_films = len(random_films)

        return len_random_films


    def repeat_films(self, list_of_repeat_films_id):

        for film in list_of_repeat_films_id:

            tconst = film[0]

            film_info = self.SQL_req.get_row_with_cinema_info_with_all_title_types(tconst)
            film_info = film_info.fetchone()

            film_id = film_info[0]
            title_type = film_info[1]
            primary_title = film_info[2]
            original_title = film_info[3]
            release_date = str(film_info[5])
            length = str(film_info[7])
            genres = film_info[8]
            rate = str(film_info[10])
            number_of_votes = str(film_info[11])
            url_for_print = URL_FOR_FILM + film_id + "/"

            self.output_for_user.output_for_film(film_id, title_type,
                                                 primary_title, original_title,
                                                 release_date, length, genres,
                                                 rate, number_of_votes, url_for_print)


    def get_remaining_films(self, list_of_repeat_films_id, title_type_of_random_films, genre_of_random_films, fame_of_random_films, search_id, number_of_random_films):

        random_films = self.SQL_req.get_list_of_random_films(genre_of_random_films, fame_of_random_films, title_type_of_random_films)
        tt_consts = []

        for const in list_of_repeat_films_id:
            const = str(const)
            const = const[2:-3]
            tt_consts.append(const)

        list_of_repeat_films_id = []
        number_of_random_films_for_cycle = 0
        sum_number_of_random_films = number_of_random_films

        for film_info in random_films:

            film_id = film_info[0]
            title_type = film_info[1]
            primary_title = film_info[2]
            original_title = film_info[3]
            release_date = str(film_info[5])
            length = str(film_info[7])
            genres = film_info[8]
            rate = str(film_info[10])
            number_of_votes = str(film_info[11])
            url_for_print = URL_FOR_FILM + film_id + "/"

            if film_id not in tt_consts:

                self.output_for_user.output_for_film(film_id, title_type,
                                                     primary_title, original_title,
                                                     release_date, length, genres,
                                                     rate, number_of_votes, url_for_print)

                number_of_random_films_for_cycle += 1

                self.SQL_req.insert_past_films(film_id, search_id)

                if number_of_random_films_for_cycle >= sum_number_of_random_films:

                    continue_question = self.clarifying_question.continue_question()

                    if continue_question == "q":
                        print("I'll be back")
                        break
                    else:
                        sum_number_of_random_films += number_of_random_films
                        continue


if __name__ == '__main__':

    first_start()
    entry_question()
